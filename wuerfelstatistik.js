// 1. JavaScript-Konstanten für HTML-Elemente
const inputAnzahlWuerfe = document.querySelector('#inputAnzahlWuerfe');
const pAnzahlWuerfe = document.querySelector('#pAnzahlWuerfe');

const buttonWuerfeln = document.querySelector('#buttonWuerfeln');

const td1 = document.querySelector('#td1');
const td2 = document.querySelector('#td2');
const td3 = document.querySelector('#td3');
const td4 = document.querySelector('#td4');
const td5 = document.querySelector('#td5');
const td6 = document.querySelector('#td6');

// 2. Daten
const DEFAULT_ANZAHL_WUERFE = 60;
let anzahlWuerfe = DEFAULT_ANZAHL_WUERFE;
const anzahl = [0, 0, 0, 0, 0, 0];

// 3. Funktionsdefinitionen
// M O D E L
const reset = () => {
    anzahl.fill(0);
}

const wuerfeln = () => {
    // würfeln
    for (let i = 0; i < anzahlWuerfe; i++) {
        const wuerfel = Math.ceil(Math.random() * 6);
        anzahl[wuerfel - 1]++;
    }
}

// V I E W
const displayAnzahlen = () => {
    // Anzahlen ausgeben
    td1.textContent = anzahl[0];
    td2.textContent = anzahl[1];
    td3.textContent = anzahl[2];
    td4.textContent = anzahl[3];
    td5.textContent = anzahl[4];
    td6.textContent = anzahl[5];
}

const displayAnzahlWuerfe = () => {
    pAnzahlWuerfe.textContent = anzahlWuerfe;
}

const changeAnzahlWuerfe = () => {
    anzahlWuerfe = inputAnzahlWuerfe.value;
}

// E V E N T S
const inputAnzahlWuerfeInput = () => {
    changeAnzahlWuerfe();
    displayAnzahlWuerfe();
}

const buttonWuerfelnClick = () => {
    reset();
    wuerfeln();
    displayAnzahlen();
}

// 4. Eventlistener
buttonWuerfeln.addEventListener('click', buttonWuerfelnClick);
inputAnzahlWuerfe.addEventListener('input', inputAnzahlWuerfeInput);

// 5. Start
inputAnzahlWuerfe.value = DEFAULT_ANZAHL_WUERFE;
displayAnzahlWuerfe();
reset();
wuerfeln();
displayAnzahlen();
